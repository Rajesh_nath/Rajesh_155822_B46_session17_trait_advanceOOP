<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/29/2017
 * Time: 8:47 PM
 */

interface canfly{
     public function fly();
}

interface canSwim{
    public function swim();
}



class bird{
    public function info(){
        echo "I am $this->name!<br>I am a Bird";
    }
}

class Dove extends Bird implements canfly{
    public $name="Dove";
    public function fly(){
        echo "I can Fly...<br>";
    }

}

class Penguin extends Bird implements canSwim
{
    public $name = "Penguin";

    public function swim()
    {
        echo "I can Swim...<br>";
    }
}

class Duck extends Bird implements canSwim,canFly{
    public $name="Duck";
    public function fly(){
        echo "I can Fly...<br>";
    }

    public function swim()
    {
        echo "I can Swim...<br>";
    }
}

function describe ($bird){
    $bird->info();

    if($bird instanceof canFly){
        $bird->fly();
    }
    if($bird instanceof canSwim){
        $bird->swim();
    }

}

describe(new Dove);
describe(new Penguin());
describe(new Duck());

