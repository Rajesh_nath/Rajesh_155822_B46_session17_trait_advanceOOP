<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/29/2017
 * Time: 7:35 PM
 */

trait A{
    public $a;
    public  function doSomething(){
        echo "doing something inside".__TRAIT__."<br>";

    }

}
trait B{
    public $b;
    public  function doSomething(){
        echo "doing something inside".__TRAIT__."<br>";

    }

}

class AB{
    use A,B;
}

class BA{
    use A,B;
}

$objAB = new AB();
$objAB->doSomething();
