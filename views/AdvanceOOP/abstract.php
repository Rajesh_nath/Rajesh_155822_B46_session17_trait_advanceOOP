<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/29/2017
 * Time: 8:34 PM
 */

abstract class MyAbstractClass
{
    abstract protected function ForceMethod1();
    abstract public function forceMethod2();

    public $a,$b,$c;

    public function normalMethod(){
        echo "inside a ".__METHOD__."<br>";
    }
}

class MyClass extends MyAbstractClass{
    public function ForceMethod1()
    {
        echo "inside a ".__METHOD__."<br>";
    }

    public function forceMethod2()
    {
        echo "inside a ".__METHOD__."<br>";
    }
}

$obj = new MyClass();
$obj->ForceMethod1();
$obj->forceMethod2();